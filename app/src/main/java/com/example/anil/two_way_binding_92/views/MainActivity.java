package com.example.anil.two_way_binding_92.views;

// not working after i made change

import android.databinding.BindingAdapter;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.example.anil.two_way_binding_92.R;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
// Inflate view and obtain an instance of the binding class.
        ActivityMainBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

         //Assign the component to a property in the binding class.
        binding.setViewModel(userModel);

    }

    @BindingAdapter({"toastMessage"})
    public static void runMe1(View view, String message) {
        if (message != null)
            Toast.makeText(view.getContext(), message, Toast.LENGTH_SHORT).show();
    }
}
