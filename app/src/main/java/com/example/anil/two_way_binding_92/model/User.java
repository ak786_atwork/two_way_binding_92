package com.example.anil.two_way_binding_92.model;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Patterns;

public class User {

    @NonNull
    private String mEmail;
    @NonNull
    private String mPassword;

    public User(@NonNull String mEmail, @NonNull String mPassword) {
        this.mEmail = mEmail;
        this.mPassword = mPassword;
    }

    @NonNull
    public String getEmail() {
        return mEmail;
    }

    public void setEmail(@NonNull String mEmail) {
        this.mEmail = mEmail;
    }

    @NonNull
    public String getPassword() {
        return mPassword;
    }

    public void setPassword(@NonNull String mPassword) {
        this.mPassword = mPassword;
    }

    //this is optional
    public boolean isInputDataValid()
    {
        boolean result = !TextUtils.isEmpty(getEmail()) && Patterns.EMAIL_ADDRESS.matcher(getEmail()).matches() && getPassword().length() > 5;
        return result;
    }
}
