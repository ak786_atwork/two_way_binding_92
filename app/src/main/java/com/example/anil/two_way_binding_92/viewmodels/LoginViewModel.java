package com.example.anil.two_way_binding_92.viewmodels;




import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.example.anil.two_way_binding_92.BR;
import com.example.anil.two_way_binding_92.model.User;

public class LoginViewModel extends BaseObservable {

    private User user;

    private String successMessage = "Login was successful";
    private String errorMessage = "Email or Password not valid";

   @Bindable
    public String toastMessage = null;

    public LoginViewModel() {
        this.user = new User("","");
    }

    public String getToastMessage() {
        return toastMessage;
    }


    private void setToastMessage(String toastMessage) {

        this.toastMessage = toastMessage;
       notifyPropertyChanged(BR.toastMessage);
    }

    public void afterEmailTextChanged(CharSequence s) {
        user.setEmail(s.toString());
    }

    public void afterPasswordTextChanged(CharSequence s) {
        user.setPassword(s.toString());
    }

    public void onLoginClicked() {
        if (user.isInputDataValid())
            setToastMessage(successMessage);
        else
            setToastMessage(errorMessage);
    }
}
